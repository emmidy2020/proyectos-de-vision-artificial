import cv2

face_cascade = cv2.CascadeClassifier('haarcascade_trontalface_default.xml')

cap = cv2.VideoCapture(0)

while True:
    _, img = cap.read()
    gray = cv2.cvtColor (img, cv2.COLOR_BCR2GRAY)
    faces = face_cascade.detectMUltiscale(gray, 1.1, 4)
    for (x, Y, w, h) in faces:
        cv2. rectangle(img, (x, y), (x+W, y+h), (0, 255, 0), 2)
    cv2. inshow(' img', img) 
    k = cv2. waitKey(30)
   
    if k == 27 : # 27 es el ascii para esc
        break
cap.release ()